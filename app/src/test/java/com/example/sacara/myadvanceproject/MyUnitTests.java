package com.example.sacara.myadvanceproject;

import com.example.sacara.myadvanceproject.api.LoginResult;
import com.example.sacara.myadvanceproject.api.Note;
import com.example.sacara.myadvanceproject.api.RegisterResult;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Response;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class MyUnitTests {
    static API api;
    static String username;
    static String password;
    static long userId;

    @BeforeClass
    public static void setup() throws IOException
    {
        api = Globals.getAPI();

        Random random = new Random();
        String randomName = "test"+random.nextInt();
        username = randomName;
        password = "123";
        Call<RegisterResult> call = api.doRegister("TesUser",randomName,password,"test@gmail.com");
        Response<RegisterResult> result = call.execute();
        userId = result.body().userId;
    }

    @Test
    public void login_user() throws IOException
    {
        Response<LoginResult> login = api.doLogin(username, password).execute();
        LoginResult result = login.body();

        Assert.assertNotNull(result);

        //this user id should be the same from the registered user id in setup function
        Assert.assertEquals(result.userId,userId);
    }

    @Test
    public void createNote() throws IOException
    {
        Response<String> res = api.addNote("testnote", "textcontents", Note.TEXT).execute();
        Response<List<Note>> notesResult = api.getNotes().execute();
        List<Note> notes = notesResult.body();

        Assert.assertNotNull(notes);
        //if notes size is 0 then the note wasnt created
        Assert.assertNotSame(0, notes.size());
    }

    @Test
    public void delete_Note() throws Exception {

        Response<String> res = api.addNote("testnote", "text contents", Note.TEXT).execute();
        Response<List<Note>> notesResult = api.getNotes().execute();
        List<Note> notes = notesResult.body();

        Assert.assertNotNull(notes);

        //this is the size before the note is deleted
        int sizeBefore = notes.size();
        Assert.assertNotSame(0, sizeBefore);

        Note note = notes.get(0);
        //delete note
        api.deleteNote(note.note_id).execute();

        //get backlist
        notesResult = api.getNotes().execute();
        notes = notesResult.body();

        //size after should be one less than size before
        int sizeAfter= notes.size();
        Assert.assertEquals(sizeBefore - 1, sizeAfter);
    }
}