package com.example.sacara.myadvanceproject.api;


public class RegisterResult {
    public boolean success;
    public String errorMessage;
    public Long userId;
}
