package com.example.sacara.myadvanceproject;

import android.content.Context;
import android.os.Bundle;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sacara.myadvanceproject.api.Event;
import com.example.sacara.myadvanceproject.api.Invitation;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class Event_preview extends Activity implements Callback<Event> {

    InviteAdapter adapter;
    Event event;

    EditText name,location,time,date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_preview);

        adapter = new InviteAdapter(this,new ArrayList<Invitation>());

        ListView l = (ListView)findViewById(R.id.invitedlist);
        l.setAdapter(adapter);

        name = (EditText)findViewById(R.id.eteventname);
        location = (EditText)findViewById(R.id.etlocation);
        date = (EditText)findViewById(R.id.date);
        time = (EditText)findViewById(R.id.timeview);


        int pos = getIntent().getIntExtra("event",-1);
        if(pos==-1)
        {
            this.finish();
            return;
        }
        event = Globals.events.get(pos);

        showEvent(event);
        //Reload list note to shows changes
        loadEvent();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void loadEvent()
    {//load notes from database
        API api = Globals.getAPI();
        Call<Event> call = api.getEvent(event.event_id);
        call.enqueue(this);
    }

    private void showEvent(Event event)
    {
        name.setText(event.title);
        location.setText(event.location);
        date.setText(event.date);
        time.setText(event.time);

        adapter.clear();
        if(event.invitations!=null)
        {
            for(Invitation i:event.invitations){
                adapter.add(i);// add new item to list
            }
        }

    }

    @Override
    public void onResponse(Call<Event> call,Response<Event> response) {
        setProgressBarIndeterminateVisibility(false);

        Event event = response.body();
        showEvent(event);


    }

    @Override //exception handling
    public void onFailure(Call<Event> call,Throwable t) {
        Toast.makeText(Event_preview.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    }

    public class InviteAdapter extends ArrayAdapter<Invitation> {
        public InviteAdapter(Context context, ArrayList<Invitation> users) {
            super(context, 0, users);
        }
        //populate array using adapter
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            Invitation invite = getItem(position);

            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.invite_item, parent, false);
            }

            TextView title = (TextView) convertView.findViewById(R.id.title);
            TextView email = (TextView) convertView.findViewById(R.id.email);
            TextView status = (TextView) convertView.findViewById(R.id.status);

            title.setText(invite.name);
            email.setText(invite.email);
            switch(invite.status)
            {
                case 0:
                    status.setText("unseen");
                    break;
                case 1:
                    status.setText("seen");
                    status.setTextColor(getResources().getColor(R.color.colorPrimary));

                    break;
                case 2:
                    status.setText("attending");
                    status.setTextColor(getResources().getColor(R.color.green));
                    break;
                case 3:
                    status.setText("not attending");
                    status.setTextColor(getResources().getColor(R.color.red));
                    break;
            }

            //date.setText(user.hometown);
            //date.setText("Jan 5");

            return convertView;
        }
    }

}
