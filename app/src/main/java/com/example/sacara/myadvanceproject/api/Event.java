package com.example.sacara.myadvanceproject.api;

import java.util.List;

public class Event {
    public int event_id;
    public String title;
    public String agenda;
    public String location;
    public String date;
    public String time;
    public List<Invitation> invitations;
}
