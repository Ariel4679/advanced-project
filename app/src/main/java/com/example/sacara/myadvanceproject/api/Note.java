package com.example.sacara.myadvanceproject.api;

import java.util.Date;

/**
 * Created by Peta-Gaye on 1/4/2016.
 */
public class Note {
    public int note_id;
    public String title;
    public String contents;
    public Date date;
    public int type;

    public static final int TEXT = 1;
    public static final int RECORDING = 2;
}
