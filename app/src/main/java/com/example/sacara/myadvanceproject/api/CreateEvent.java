package com.example.sacara.myadvanceproject.api;

import java.util.List;

/**
 * Created by Peta on 4/18/2016.
 */
public class CreateEvent {
    public int event_id;
    public String title;
    public String agenda;
    public String location;
    public String date;
    public String time;
    public String invitations;
}
