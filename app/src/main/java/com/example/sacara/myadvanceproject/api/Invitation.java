package com.example.sacara.myadvanceproject.api;

/**
 * Created by Peta-Gaye on 12/4/2016.
 */
public class Invitation {
    public int invitation_id;
    public String name;
    public String email;
    public int status;
}
