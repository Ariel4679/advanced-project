package com.example.sacara.myadvanceproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button blogout;
    EditText etname, etusername, etemail;
    UserLocalStore userLocalStore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etname = (EditText) findViewById(R.id.etname);
        etusername= (EditText) findViewById(R.id.etusername);
        etemail= (EditText) findViewById(R.id.etemail);


        blogout = (Button) findViewById(R.id.blogout);

        blogout.setOnClickListener(this);

        userLocalStore = new UserLocalStore(this);

    }
    @Override
    protected void onStart()
    {
        super.onStart();
        if(authenticate()==true)
        {
            displayUserDetails();
        }
        else
        {
            startActivity(new Intent(MainActivity.this,Login.class ));
        }
    }

    private boolean authenticate()
    {
        return userLocalStore.getUserLoggedIn();
    }

    private void displayUserDetails()
    {
        User user = userLocalStore.getLoggedInUser();

        etusername.setText(user.username);
        etname.setText(user.name);
        etemail.setText(user.email);
    }
    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.blogout:
                userLocalStore.clearUserData();
                userLocalStore.setUserLoggedIn(false);

                startActivity(new Intent(this, Login.class));
                break;
        }
    }
}
