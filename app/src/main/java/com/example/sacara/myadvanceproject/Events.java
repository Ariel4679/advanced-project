package com.example.sacara.myadvanceproject;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.net.Uri;
import android.util.Log;

import com.example.sacara.myadvanceproject.api.CreateEvent;
import com.example.sacara.myadvanceproject.api.Event;
import com.example.sacara.myadvanceproject.api.Invitation;
import com.example.sacara.myadvanceproject.api.Note;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class Events extends AppCompatActivity implements View.OnClickListener, Callback<String> {
    //Buttons and Edit Text Declarations
    Button dateButton,timeButton, emailButton,addPerson,home,addEventButton;
    EditText eventName, eventLocation,timeview, dateview;
   //Declaration of variables
    int hour_x, minute_x;
    int day_x, month_x, year_x;
    static final int DIALOG_ID = 0;
    static final int DIALOG_ID2 = 1;
    private String format = "";
    final Context context = this;

    InviteAdapter adapter;
    ArrayList<Invitation> invites;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


//Button and text view listeners
        dateButton = (Button) findViewById(R.id.dateButton);
        timeButton = (Button) findViewById(R.id.timeButton);
       // home = (Button) findViewById(R.id.home);

        addPerson = (Button) findViewById(R.id.addbutton);
        eventName = (EditText) findViewById(R.id.eteventname);
        eventLocation = (EditText) findViewById(R.id.etlocation);
        timeview = (EditText) findViewById(R.id.timeview);
        dateview = (EditText) findViewById(R.id.dateview);

//Button and text view binders
        dateButton.setOnClickListener(this);
        timeButton.setOnClickListener(this);
        addPerson.setOnClickListener(this);
        //home.setOnClickListener(this);

        //Initialize variables for calendar instance
        final Calendar cal = Calendar.getInstance();
        year_x= cal.get(Calendar.YEAR);
        month_x= cal.get(Calendar.MONTH);
        day_x= cal.get(Calendar.DAY_OF_MONTH);

       FloatingActionButton emailButton = (FloatingActionButton) findViewById(R.id.emailbutton);
        emailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendEmail();
            }
        });

        invites = new ArrayList<Invitation>();
        adapter = new InviteAdapter(this,invites);
        ListView l = (ListView)findViewById(R.id.emailList);
        l.setAdapter(adapter);

        addEventButton = (Button) findViewById(R.id.createbutton);
        addEventButton.setOnClickListener(this);
    }

    @Override
    public void onResponse(Call<String> call,Response<String> response) {
        //setProgressBarIndeterminateVisibility(false);

        Toast.makeText(this,"Event Created",Toast.LENGTH_SHORT).show();
    }

    @Override //exception handling
    public void onFailure(Call<String> call,Throwable t) {
        Toast.makeText(Events.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        t.printStackTrace();
    }

    @Override
    public void onClick(View v) {//switch case to distinguish button clicks
        switch (v.getId())
        {
            case R.id.dateButton:
                showDialogOnButtonClick();
                break;

            case R.id.timeButton:
                showTimePickerDialog();
                break;
            case R.id.addbutton:
                inviteMembers();
                break;
           // case R.id.home:
            //    startActivity(new Intent(this,Options.class));
            //    break;
            case R.id.createbutton:
                createEvent();
                break;
        }
    }
    public void createEvent()
    {
        Map<String, String> event = new HashMap<String, String>();
        event.put("title",eventName.getText().toString());
        event.put("location", eventLocation.getText().toString());
        event.put("date", dateview.getText().toString());
        event.put("time", timeview.getText().toString());
        event.put("agenda","");

        String json = "[";
        for(int i=0;i<invites.size();i++)
        {
            Invitation invite = invites.get(i);
            json += "{\"name\":\""+invite.name+"\",\"email\":\""+invite.email+"\"}";
            if(i!=invites.size()-1)
                json+=",";
        }
        json+="]";
        event.put("invitations",json);

        API api = Globals.getAPI();
        Call<String> call = api.createEvent(Globals.userId,event);
        call.enqueue(this);
    }
    public void inviteMembers()
    {
        // custom dialog
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.custom__dialog);
        dialog.setTitle("Invite Person");


        Button dialogButton = (Button) dialog.findViewById(R.id.okbutton);
        // if button is clicked, close the custom dialog
       dialogButton.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               dialog.dismiss();

               Invitation invite = new Invitation();
               EditText name = (EditText)dialog.findViewById(R.id.etmembersname);
               EditText email = (EditText)dialog.findViewById(R.id.etmemberemail);
               invite.name = name.getText().toString();
               invite.email = email.getText().toString();

               invites.add(invite);
               adapter.notifyDataSetChanged();
           }
       });


        Button dialog2Button = (Button) dialog.findViewById(R.id.cancelbutton);
        // if button is clicked, close the custom dialog
        dialog2Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void showDialogOnButtonClick() {
                showDialog(DIALOG_ID);
    }//calling function
    public void showTimePickerDialog(){
                showDialog(DIALOG_ID2);
    }//calling function

    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == 0)
            return new DatePickerDialog(this, dPickerListener, year_x, month_x, day_x);
        else if(id == 1){
            return new TimePickerDialog(this, kTimePickerListener, hour_x, minute_x, false);
        }
        else{
        return null;}
    }

    //Date picker dialog function
    private DatePickerDialog.OnDateSetListener dPickerListener =
            new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    year_x = year;
                    month_x = monthOfYear + 1;
                    day_x = dayOfMonth;
                    Toast.makeText(Events.this, year_x + "/ " + month_x +"/ "+ day_x , Toast.LENGTH_SHORT).show();
                    dateview.setText(year_x + "/ " + month_x +"/ "+ day_x);
                }

            };

    //Time picker dialog function
    private TimePickerDialog.OnTimeSetListener kTimePickerListener =
            new TimePickerDialog.OnTimeSetListener() {

                @Override
                public void onTimeSet(TimePicker view, int hourOfday, int minutes) {
                    hour_x = hourOfday;
                    minute_x = minutes;

                    if (hour_x == 0) {
                        hour_x += 12;
                        format = "AM";
                    }
                    else if (hour_x == 12) {
                        format = "PM";
                    } else if (hour_x > 12) {
                        hour_x -= 12;
                        format = "PM";
                    } else {
                        format = "AM";
                    }
                    Toast.makeText(Events.this, hour_x + " : " + minute_x + " " + format , Toast.LENGTH_SHORT).show();
                    timeview.setText(hour_x + " : " + minute_x + " " + format);//Output selected time to edit text view
                }
            };

    protected void sendEmail() {
        Log.i("Send email", "");
        String[] TO = {""};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);//Email intent

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Invitation to Scheduled Meeting");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Name of meeting : " + eventName.getText().toString() + "\n\nLocation:  "
                +  eventLocation.getText().toString() + "\n\nDate: "+ year_x + "/ " + month_x +"/ "+ day_x + "\n\nTime: "
                +  hour_x + " : " + minute_x + " " + format + "\n\nExtra Notes: " );

    // Exception Handling
        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            finish();
            Log.i("Finished sending email", "");
        }
        catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(Events.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }


    public class InviteAdapter extends ArrayAdapter<Invitation> {
        public InviteAdapter(Context context, ArrayList<Invitation> users) {
            super(context, 0, users);
        }
        //populate array using adapter
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            Invitation invite = getItem(position);

            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.invite_item, parent, false);
            }

            TextView title = (TextView) convertView.findViewById(R.id.title);
            TextView email = (TextView) convertView.findViewById(R.id.email);
            TextView status = (TextView) convertView.findViewById(R.id.status);

            title.setText(invite.name);
            email.setText(invite.email);
            status.setText("");

            return convertView;
        }
    }

}

