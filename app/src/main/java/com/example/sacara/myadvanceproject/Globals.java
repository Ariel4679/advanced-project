package com.example.sacara.myadvanceproject;


import com.example.sacara.myadvanceproject.api.Event;
import com.example.sacara.myadvanceproject.api.Note;
import com.example.sacara.myadvanceproject.api.Recording;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Globals {
    public static String baseUrl = "http://www.ncucsprojects.com/AdProj/50120275/AdvProj/";
    public static int userId = 10;

    static List<Recording> recordings = new ArrayList<Recording>();
    static List<Event> events = new ArrayList<Event>();

    //http://stackoverflow.com/questions/27880362/retrofit-1-9-with-okhttp-2-2-and-interceptors
    //http://stackoverflow.com/questions/32514410/logging-with-retrofit-2
    public static API getAPI()
    {
        /*
        OkHttpClient client = new OkHttpClient();
        client.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                return null;
            }
        });
        */

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        //OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(baseUrl)
                //.baseUrl("http://www.ncucsprojects.com/AdProj/50120275/AdvProj/")
                //.baseUrl("http://192.168.0.17/Advproj/")
                .addConverterFactory(GsonConverterFactory.create())

                .build();

        API api = retrofit.create(API.class);
        return api;
    }
}
