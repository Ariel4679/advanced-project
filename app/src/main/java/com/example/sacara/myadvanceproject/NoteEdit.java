package com.example.sacara.myadvanceproject;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.example.sacara.myadvanceproject.api.Note;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
//import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;


public class NoteEdit extends Activity implements View.OnClickListener, Callback<String> {

    ImageButton record;
    Button save;
    EditText preview;
    String note, title;


    final int SPEECHTOTEXT=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_edit);

        preview = (EditText) findViewById(R.id.quoteTextArea);
        save = (Button) findViewById(R.id.save);
        record = (ImageButton) findViewById(R.id.record);



        save.setOnClickListener(this);
        record.setOnClickListener(this);

    }

    void saveNote ()
    {

        title = ((EditText)findViewById(R.id.title)).getText().toString();
        String contents = ((EditText)findViewById(R.id.quoteTextArea)).getText().toString();
        //String title = "test";
        //String contents = "test contents";
        API api = Globals.getAPI();

        Call<String> call = api.addNote(title, contents, Note.TEXT);
        //Log.d("app","fetching notes");
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<String> call,Response<String> response) {
        Toast.makeText(this,"note saved",Toast.LENGTH_SHORT).show();

        this.finish();
    }

    @Override
    public void onFailure(Call<String> call,Throwable t) {
        Toast.makeText(this,"unable to save message",Toast.LENGTH_SHORT).show();

        this.finish();
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.delete:
                startActivity(new Intent(this, Events.class));

                break;

            case R.id.save:
                saveNote();

                break;
            case R.id.record:
                startActivityForResult(new Intent(this, TextToSpeech.class), SPEECHTOTEXT);

                break;

        }
    }

    @Override
    protected void onActivityResult(int requestCode,int resultCode,Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==SPEECHTOTEXT)
        {
            if(resultCode==Activity.RESULT_OK)
            {
                String text = data.getStringExtra("text");

                //adds text at cursor
                //http://stackoverflow.com/questions/3609174/android-insert-text-into-edittext-at-current-position
                int start = Math.max(preview.getSelectionStart(), 0);
                int end = Math.max(preview.getSelectionEnd(), 0);
                preview.getText().replace(Math.min(start, end), Math.max(start, end),
                        text, 0, text.length());
            }
        }
    }

}
