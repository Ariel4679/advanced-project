package com.example.sacara.myadvanceproject.api;


public class LoginResult {
    public boolean success;
    public String errorMessage;
    public int userId;
}
