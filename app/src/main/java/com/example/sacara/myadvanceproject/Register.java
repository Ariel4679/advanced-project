package com.example.sacara.myadvanceproject;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sacara.myadvanceproject.api.LoginResult;
import com.example.sacara.myadvanceproject.api.RegisterResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class Register extends AppCompatActivity implements View.OnClickListener, Callback<RegisterResult> {

    Button bregister;
    EditText etname, etusername, etemail, etpassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        etname = (EditText) findViewById(R.id.etname);
        etusername= (EditText) findViewById(R.id.etusername);
        etemail= (EditText) findViewById(R.id.etemail);
        etpassword = (EditText) findViewById(R.id.etpassword);

        bregister = (Button) findViewById(R.id.bregister);

        bregister.setOnClickListener(this);

    }
    public void regx(View view)
    {//DECLARATIONS
        String r, n, p;
        r = new String("Register");
        n = new String("root");
        p = new String("root");
        //BackgroundTasks b = new BackgroundTasks(this);

        //b.execute(r,n,p);
    }
    @Override
    public void onResponse(Call<RegisterResult>  call,Response<RegisterResult> response) {
        RegisterResult result = response.body();

        if(result.success)
        {
            startActivity(new Intent(this,Options.class));
        }
        else
        {
            Toast.makeText(this, result.errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onFailure(Call<RegisterResult>  call,Throwable t) {
        Toast.makeText(this,"Error logging in. Please check your internet connection.", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.bregister:

                String name = etname.getText().toString();
                String username = etusername.getText().toString();
                String email = etemail.getText().toString();
                String password = etpassword.getText().toString();

                //User registeredData = new User(name, username, email, password);
                //BackgroundTasks task = new BackgroundTasks(this);
                //task.execute("Register",name,username,email,password);
                API api = Globals.getAPI();
                Call<RegisterResult> result = api.doRegister(name,username,password,email);
                result.enqueue(this);

                break;
        }
    }
}
