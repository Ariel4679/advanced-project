package com.example.sacara.myadvanceproject;

import com.example.sacara.myadvanceproject.api.CreateEvent;
import com.example.sacara.myadvanceproject.api.Event;
import com.example.sacara.myadvanceproject.api.LoginResult;
import com.example.sacara.myadvanceproject.api.Note;
import com.example.sacara.myadvanceproject.api.Recording;
import com.example.sacara.myadvanceproject.api.RegisterResult;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface API {

    @FormUrlEncoded
    @POST("login.php")
    Call<LoginResult> doLogin(@Field("username") String username,@Field("password") String password);

    @FormUrlEncoded
    @POST("register.php")
    Call<RegisterResult> doRegister(@Field("name") String name,@Field("username") String username,@Field("password") String password,@Field("email") String email);

    @GET("notes.php")
    Call<List<Note>> getNotes();

    @FormUrlEncoded
    @POST("add_note.php")
    Call<String> addNote(@Field("title") String title,@Field("contents") String contents,@Field("type") int type);

    @GET("delete_note.php")
    Call<String > deleteNote(@Query("note_id") int noteId);

    @FormUrlEncoded
    @POST("update_note.php")
    Call<String> updateNote(@Field("note_id") int noteId,@Field("title") String title,@Field("contents") String contents);

    @GET("recordings.php")
    Call<List<Recording>> getRecordings(@Query("user_id") int userId);

    @Multipart
    @POST("add_recording.php")
    Call<String> addRecording(@Part("user_id") int userId,@Part("title") String title,@Part MultipartBody.Part contents);

    @Multipart
    @POST("test_upload.php")
    Call<ResponseBody> testUpload(@Query("get_var") String query,@Part("description") String description,
                              @Part MultipartBody.Part file);

    @GET("delete_recording.php")
    Call<String> deleteRecording(@Query("recording_id") int recordingId);

    @FormUrlEncoded
    @POST("update_recording.php")
    Call<String> updateRecording(@Field("recording_id") int recordingId,@Field("title") String title);

    @FormUrlEncoded
    @POST("add_event.php")
    Call<String> createEvent(@Query("user_id") int userId,@FieldMap Map<String, String> event);

    @GET("events.php")
    Call<List<Event>> getEvents(@Query("user_id") int userId);

    @GET("event.php")
    Call<Event> getEvent(@Query("event_id") int eventId);
}
