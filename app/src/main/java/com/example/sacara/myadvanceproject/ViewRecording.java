package com.example.sacara.myadvanceproject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sacara.myadvanceproject.api.Recording;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewRecording extends Activity  implements  View.OnClickListener{

    TextView title;
    Button delete, save;
    Button play,stop;
    API api;
    Recording rec;
    ProgressDialog pd;
    MediaPlayer mediaPlayer;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_recording);

        delete = (Button) findViewById(R.id.delete);
        save = (Button) findViewById(R.id.save);

        play = (Button) findViewById(R.id.okay_button);
        stop = (Button) findViewById(R.id.cancel_button);

        delete.setOnClickListener(this);
        save.setOnClickListener(this);
        play.setOnClickListener(this);
        stop.setOnClickListener(this);

        int pos = getIntent().getIntExtra("recording",-1);
        if(pos==-1)
        {
            this.finish();
            return;
        }

        rec = Globals.recordings.get(pos);
        title = (TextView) findViewById(R.id.title);
        title.setText(rec.title);

        api = Globals.getAPI();
        pd = new ProgressDialog(this);

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try{
            String url = Globals.baseUrl+"recordings/"+rec.file_name;
            Log.v("app",url);
            mediaPlayer.setDataSource(url);
            mediaPlayer.prepare();
        }catch(IOException e)
        {

        }

    }

    void deleteRecording()
    {
        api.deleteRecording(rec.recording_id).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

            }

            @Override
            public void onFailure(Call<String> call,Throwable t) {

            }
        });
        Toast.makeText(this, "Recording Deleted", Toast.LENGTH_SHORT).show();
        this.finish();
    }

    void updateRecording()
    {
        pd.setMessage("updating");
        pd.show();
        api.updateRecording(rec.recording_id, title.getText().toString()).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                pd.hide();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                pd.hide();
            }
        });

        Toast.makeText(this,"Recording Updated", Toast.LENGTH_SHORT).show();
    }

    void playRecording()
    {
        mediaPlayer.start();
    }

    void stopRecording()
    {
        mediaPlayer.stop();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.delete:
                deleteRecording();
                break;

            case R.id.save:
                updateRecording();
                break;

            case R.id.okay_button:
                playRecording();
                break;

            case R.id.cancel_button:
                stopRecording();
                break;
        }
    }
}
