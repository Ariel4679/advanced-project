package com.example.sacara.myadvanceproject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sacara.myadvanceproject.api.Note;
import com.example.sacara.myadvanceproject.api.Recording;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

//http://stackoverflow.com/questions/10599578/access-contacts-and-get-email-address
public class Documents extends Activity implements Callback<List<Recording>>, View.OnClickListener {
//Declarations
    final int RECORDING_REQUEST=1;

    ListView list;
    RecordingsAdapter adapter;
    Button addrecord;

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_documents);

        //Binding buttons and list adapters
        list = (ListView)findViewById(R.id.audiolist);
        adapter = new RecordingsAdapter(this,new ArrayList<Recording>());
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Recording item = adapter.getItem(position);

                Intent intent = new Intent(getApplicationContext(), ViewRecording.class);
                intent.putExtra("recording", position);
                startActivity(intent);
            }
        });

        addrecord = (Button) findViewById(R.id.addrecord);
        addrecord.setOnClickListener(this);

        pd = new ProgressDialog(this);
    }

    @Override
    public void onClick(View v) {//On click listener function
        switch (v.getId()) {
            case R.id.addrecord:
                startActivityForResult(new Intent(this, Recordings.class), RECORDING_REQUEST);//loading new activity
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == RECORDING_REQUEST) {
            if(resultCode == Activity.RESULT_OK){
                String result=data.getStringExtra("file");

                //upload file
                API api = Globals.getAPI();

                //File file = FileUtils.getFile(this,result);
                File file = new File(result);
                RequestBody requestFile =
                        RequestBody.create(MediaType.parse("multipart/form-data"), file);
                MultipartBody.Part body =
                        MultipartBody.Part.createFormData("file", file.getName(), requestFile);


                //Call<ResponseBody> call = api.testUpload("this is a get var","this is a test file",body);
                DateFormat df = new DateFormat();
                String title = df.format("yyyy-MM-dd hh:mm:ss", new Date()).toString();
                Call<String> call = api.addRecording(Globals.userId, title, body);

                pd.setTitle("uploading");
                pd.show();
                call.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call,
                                           Response<String> response) {
                        loadRecordings();
                        pd.hide();
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Log.e("Upload error:", t.getMessage());
                        pd.hide();
                    }
                });

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }//onActivityResult

    private void loadRecordings()
    {
        API api = Globals.getAPI();
        Call<List<Recording>> call = api.getRecordings(Globals.userId);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<List<Recording>> call,Response<List<Recording>> response) {
        setProgressBarIndeterminateVisibility(false);
        //finding list view
        ListView l = (ListView)findViewById(R.id.list);

        adapter.clear();
        for(Recording rec:response.body()){
            adapter.add(rec);
        }

        Globals.recordings = response.body();
        //l.invalidate();
        //pd.hide();
    }

    @Override
    public void onFailure(Call<List<Recording>> call,Throwable t) {//Exception function
        Toast.makeText(Documents.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

        pd.hide();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        loadRecordings();
    }

    public class RecordingsAdapter extends ArrayAdapter<Recording> {
        public RecordingsAdapter(Context context, ArrayList<Recording> users) {
            super(context, 0, users);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            Recording rec = getItem(position);

            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.note_item, parent, false);
            }

            TextView title = (TextView) convertView.findViewById(R.id.textView2);
            TextView date = (TextView) convertView.findViewById(R.id.textView6);

            title.setText(rec.title);

            return convertView;
        }
    }

}
