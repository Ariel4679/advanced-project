package com.example.sacara.myadvanceproject;

/**
 * Created by Peta-Gaye on 1/17/2016.
 */
public class User {
    String name, username, email, password;

    public User(String name, String username, String email, String password)
    {
        this.name =name;
        this.username= username;
        this.email= email;
        this.password = password;
    }
    public User(String username, String password)
    {
        this.name = "";
        this.username = username;
        this.email = "";
        this.password = password;
    }

}
