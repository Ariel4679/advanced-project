package com.example.sacara.myadvanceproject;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sacara.myadvanceproject.api.Note;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ListNote extends Activity implements Callback<List<Note>> {
    Button addNote;
    String item;
    ListNote intance;

    NotesAdapter adapter;
    ArrayList<Note> notes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        requestWindowFeature(Window.FEATURE_PROGRESS);

        setContentView(R.layout.activity_list_note);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        intance = this;

        final String[] values = new String[] { "Note 1", "Note 2", "Note 3","Note 4"};

        //final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, values);
        notes = new ArrayList<>();
        adapter = new NotesAdapter(this,notes);

        ListView l = (ListView)findViewById(R.id.list);
        l.setAdapter(adapter);

        l.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Note item = adapter.getItem(position);

                Intent intent = new Intent(getApplicationContext(), NotePreviewActivity.class);
                intent.putExtra("note",position);
                startActivity(intent);
            }
        });

        addNote = (Button) findViewById(R.id.addnotebutton);

        addNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), NoteEdit.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onResume()
    {
        super.onResume();
        //Reload list note to shows changes
        loadNotes();
    }


    private void loadNotes()
    {//load notes from database
        API api = Globals.getAPI();
        Call<List<Note>> noteCall = api.getNotes();
        noteCall.enqueue(this);
    }

    @Override
    public void onResponse(Call<List<Note>> call,Response<List<Note>> response) {
        setProgressBarIndeterminateVisibility(false);

        ListView l = (ListView)findViewById(R.id.list);
        //list view
        adapter.clear();
        for(Note note:response.body()){
            adapter.add(note);// add new item to list
        }

        Notes.notes = response.body();
    }

    @Override //exception handling
    public void onFailure(Call<List<Note>> call,Throwable t) {
        Toast.makeText(ListNote.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    }

    public class NotesAdapter extends ArrayAdapter<Note> {
        public NotesAdapter(Context context, ArrayList<Note> users) {
            super(context, 0, users);
        }
        //populate array using adapter
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            Note note = getItem(position);

            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.note_item, parent, false);
            }

            TextView title = (TextView) convertView.findViewById(R.id.textView2);
            TextView date = (TextView) convertView.findViewById(R.id.textView6);

            title.setText(note.title);
            //date.setText(user.hometown);
            date.setText("Jan 5");

            return convertView;
        }
    }
}
