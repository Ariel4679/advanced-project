package com.example.sacara.myadvanceproject;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sacara.myadvanceproject.api.Note;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class NotePreviewActivity extends NoteEdit implements  View.OnClickListener{

    TextView information;
    TextView title;
    Button  delete, save;
    FloatingActionButton emailbutton;

    API api;
    Note note;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_preview);




        delete = (Button) findViewById(R.id.delete);
        save = (Button) findViewById(R.id.save);
        emailbutton = (FloatingActionButton) findViewById (R.id.emailbutton);


        delete.setOnClickListener(this);
        save.setOnClickListener(this);
        emailbutton.setOnClickListener(this);


        title = (TextView) findViewById(R.id.title);
        information = (TextView) findViewById(R.id.quoteTextArea);
        int pos = getIntent().getIntExtra("note",-1);
        if(pos==-1)
        {
            this.finish();
            return;
        }


        note = Notes.notes.get(pos);
        title.setText(note.title);
        information.setText(note.contents);


        api = Globals.getAPI();
    }

    void deleteNote()
    {
        api.deleteNote(note.note_id).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call,Response<String> response) {

            }

            @Override
            public void onFailure(Call<String> call,Throwable t) {

            }
        });
        Toast.makeText(this,"Note Deleted", Toast.LENGTH_SHORT).show();
        this.finish();
    }

    void updateNote()
    {
        api.updateNote(note.note_id, title.getText().toString(), information.getText().toString()).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call,Response<String> response) {

            }

            @Override
            public void onFailure(Call<String> call,Throwable t) {

            }
        });

        Toast.makeText(this,"Note Updated", Toast.LENGTH_SHORT).show();
    }
    protected void sendEmail() {
        Log.i("Send email", "");
        String[] TO = {""};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);//Email intent

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Minutes of Meeting");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Title : " + title.getText().toString() + "\n\n"
                +  information.getText().toString());

        // Exception Handling
        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            finish();
            Log.i("Finished sending email", "");
        }
        catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(NotePreviewActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.delete:
                deleteNote();
                break;

            case R.id.save:
                updateNote();
                break;
            case R.id.emailbutton:
                sendEmail();

                break;

        }
    }
}
