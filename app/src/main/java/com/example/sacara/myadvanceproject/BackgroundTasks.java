package com.example.sacara.myadvanceproject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by Peta on 3/5/2016.
 */
public class BackgroundTasks extends AsyncTask<String, Void, String> {
    Context ctx;
    AlertDialog alertDialog;
    String type;//login or register
    BackgroundTasks(Context ctx,String type){
        this.ctx = ctx;
        this.type = type;
    }
    @Override
    protected void onPreExecute() {
        alertDialog = new AlertDialog.Builder(ctx).create();
        alertDialog.setTitle("Login Info");
    }

    @Override
    protected String doInBackground(String... params) {
       //String url_reg = "http://www.ncucsprojects.com/AdProj/50120275/AdvProj/register.php";
        //String url_log = "http://www.ncucsprojects.com/AdProj/50120275/AdvProj/login.php";
        String url_reg = "http://192.168.0.7/Advproj/register.php";
        String url_log = "http://192.168.0.7/Advproj/login.php";
        String method = params[0];
        String responses = "";
        if(method.equals("Register"))
        {
            String name = params[1];
            String username = params[2];
            String email = params[3];
            String password = params[4];
            try {
                URL url = new URL(url_reg);

                HttpURLConnection http = (HttpURLConnection)url.openConnection();
                http.setRequestMethod("POST");
                http.setDoOutput(true);

                OutputStream op = http.getOutputStream();
                BufferedWriter bWriter = new BufferedWriter(new OutputStreamWriter(op, "UTF-8"));

                String data = URLEncoder.encode("name", "UTF-8") + "=" + URLEncoder.encode(name, "UTF-8") +"&"+
                              URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(username, "UTF-8") +"&"+
                              URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(email, "UTF-8") +"&"+
                              URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(password, "UTF-8");

                bWriter.write(data);
                bWriter.flush();
                bWriter.close();
                op.close();

                InputStream ip = http.getInputStream();
                BufferedReader bReader = new BufferedReader(new InputStreamReader(ip, "ISO-8859-1"));


                String line = "";

                while ((line = bReader.readLine()) != null) {
                    responses += line;/**/
                }
                bReader.close();
                ip.close();
                http.disconnect();

                return responses;
            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        if (method.equals("Login"))
        {
            String username = params[1];
            String password = params[2];
            try {
                URL url = new URL(url_log);

                HttpURLConnection http = (HttpURLConnection) url.openConnection();
                http.setRequestMethod("POST");
                http.setDoOutput(true);
                http.setDoInput(true);

                OutputStream op = http.getOutputStream();
                BufferedWriter bWriter = new BufferedWriter(new OutputStreamWriter(op, "UTF-8"));

                String data = URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(username, "UTF-8") + "&" +
                        URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(password, "UTF-8");

                bWriter.write(data);
                bWriter.flush();
                bWriter.close();
                op.close();

                InputStream ip = http.getInputStream();
                BufferedReader bReader = new BufferedReader(new InputStreamReader(ip, "ISO-8859-1"));


                String line = "";

                while ((line = bReader.readLine()) != null) {
                    responses += line;/**/
                }
                bReader.close();
                ip.close();
                http.disconnect();

                return responses;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String result) {
        if(result == null)
        {
            Toast.makeText(ctx, "Error- no result", Toast.LENGTH_LONG).show();
            return;
        }
        /*
        if(result.equals("Registration Successful..."))
        {
            Toast.makeText(ctx, result, Toast.LENGTH_LONG).show();
        }
        */
        else
        {
            alertDialog.setMessage(result);
            alertDialog.show();
            Toast.makeText(ctx, result, Toast.LENGTH_LONG).show();

            //if(type=="login")
            //{
                Intent intent = new Intent(ctx,Options.class);
                ctx.startActivity(intent);
            //}
            //else
            //{

            //}


        }
    }
}
