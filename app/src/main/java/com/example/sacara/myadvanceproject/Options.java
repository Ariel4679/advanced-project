package com.example.sacara.myadvanceproject;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;

public class Options extends AppCompatActivity implements View.OnClickListener{
    Toolbar toolbar;
    Button eventBtn, minuteBtn, browseBtn, noteBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        eventBtn = (Button) findViewById(R.id.eventButton);
        browseBtn = (Button) findViewById(R.id.reposButton);
        noteBtn = (Button) findViewById(R.id.notesButton);


        eventBtn.setOnClickListener(this);
        browseBtn.setOnClickListener(this);
        noteBtn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.eventButton:
                startActivity(new Intent(this, EventCalendar.class));

                break;

            case R.id.notesButton:
                startActivity(new Intent(this,
                        ListNote.class));

                break;

            case R.id.reposButton:
                startActivity(new Intent(this,
                        Documents.class));

                break;
        }
    }
}
