package com.example.sacara.myadvanceproject;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sacara.myadvanceproject.api.LoginResult;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class Login extends AppCompatActivity implements View.OnClickListener, Callback<LoginResult> {

    //Declarations
    Button bLogin;
    EditText etusername, etpassword;
    TextView tvRegisterLink;

    UserLocalStore userLocalStore;
@Override
    protected void onCreate(Bundle savedInstanceState){
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);
    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//Binders
    etusername = (EditText) findViewById(R.id.etusername);
    etpassword = (EditText) findViewById(R.id.etpassword);
    bLogin = (Button) findViewById(R.id.blogin);
    tvRegisterLink = (TextView) findViewById(R.id.tvRegisterLink);
//Listners
    if(this.bLogin != null) {
        bLogin.setOnClickListener(this);
    }
    if(this.tvRegisterLink != null) {
        tvRegisterLink.setOnClickListener(this);
    }
    userLocalStore = new UserLocalStore(this);

    }

    @Override
    public void onResponse(Call<LoginResult> call,Response<LoginResult> response) {
        LoginResult result = response.body();

        if(!response.isSuccessful())
        {//Display Error
            Toast.makeText(this,"error logging in", Toast.LENGTH_LONG).show();
            Log.d("app",response.message());
            return;
        }

        if(result.success)
        {
            Globals.userId = result.userId;
            startActivity(new Intent(this, Options.class));//Start new activity
            this.finish();
        }
        else
        {
            Toast.makeText(this, result.errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    @Override
     public void onFailure(Call<LoginResult> call,Throwable t) {//Exception Handling
        Toast.makeText(this,"Error logging in. Please check your internet connection.", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.blogin:
                //startActivity(new Intent(this, Options.class));
               // User user = new User(null, null);

               // userLocalStore.storeUserData(user);
               // userLocalStore.setUserLoggedIn(true);

                String username = etusername.getText().toString();
                String password = etpassword.getText().toString();

                Boolean success = false;

                //BackgroundTasks task = new BackgroundTasks(this);
                //task.execute("Login",username, password);
                API api = Globals.getAPI();
                Call<LoginResult> result = api.doLogin(username,password);
                result.enqueue(this);

                break;

            case R.id.tvRegisterLink:
                startActivity(new Intent(this, Register.class));
                break;

        }
    }

}
