package com.example.sacara.myadvanceproject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sacara.myadvanceproject.api.Event;
import com.example.sacara.myadvanceproject.api.Invitation;
import com.example.sacara.myadvanceproject.api.Note;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class EventCalendar extends Activity implements View.OnClickListener, Callback<List<Event>> {
    Button addEvent;
    EventsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_calendar);

        //binding button to onclick listeners
        addEvent= (Button) findViewById(R.id.addnewbutton);
        addEvent.setOnClickListener(this);

        adapter = new EventsAdapter(this,new ArrayList<Event>());

        ListView l = (ListView)findViewById(R.id.list);
        l.setAdapter(adapter);

        l.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Event item = adapter.getItem(position);

                Intent intent = new Intent(getApplicationContext(), Event_preview.class);
                intent.putExtra("event", position);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addnewbutton:
                startActivity(new Intent(this, Events.class));
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        //Reload list note to shows changes
        loadEvents();
    }


    private void loadEvents()
    {//load notes from database
        API api = Globals.getAPI();
        Call<List<Event>> call = api.getEvents(Globals.userId);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<List<Event>> call,Response<List<Event>> response) {
        setProgressBarIndeterminateVisibility(false);

        ListView l = (ListView)findViewById(R.id.list);
        //list view
        adapter.clear();
        for(Event evt:response.body()){
            adapter.add(evt);// add new item to list
        }

        Globals.events = response.body();
    }

    @Override //exception handling
    public void onFailure(Call<List<Event>> call,Throwable t) {
        Toast.makeText(EventCalendar.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    }

    public class EventsAdapter extends ArrayAdapter<Event> {
        public EventsAdapter(Context context, ArrayList<Event> users) {
            super(context, 0, users);
        }
        //populate array using adapter
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            Event event = getItem(position);

            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.note_item, parent, false);
            }

            TextView title = (TextView) convertView.findViewById(R.id.textView2);
            TextView date = (TextView) convertView.findViewById(R.id.textView6);

            title.setText(event.title);
            //date.setText(user.hometown);
            //date.setText("Jan 5");

            return convertView;
        }
    }
}
